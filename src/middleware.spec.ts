import { Context } from 'koa';

import { RequestTimeoutConfig } from './config';
import createRequestTimeoutMiddleware, { clearRequestTimeout, RequestTimeoutError, timeoutPromise } from './middleware';

const TIMEOUT_HTTP_STATUS = 504;
const TIMEOUT_IN_MS = 10;

function getMockConfig(): RequestTimeoutConfig {
  return {
    timeoutHttpStatus: TIMEOUT_HTTP_STATUS,
    timeoutInMs: TIMEOUT_IN_MS
  };
}

describe('requestTimeoutMiddleware', () => {
  beforeEach(() => jest.useFakeTimers());

  describe('timeoutPromise', () => {
    it('returns a Promise calling setTimeout', async () => {
      const ctx = { requestTimeout: { hasTimedOut: false } } as Context;
      const p = timeoutPromise(ctx, TIMEOUT_IN_MS);

      await expect(p).toBeInstanceOf(Promise);
      expect(setTimeout).toHaveBeenCalledTimes(1);
      expect(setTimeout).toHaveBeenCalledWith(expect.any(Function), TIMEOUT_IN_MS);
    });

    it('rejects Promise if missing context', async () => {
      const ctx = {} as Context;
      const p = timeoutPromise(ctx, TIMEOUT_IN_MS);
      jest.runAllTimers();

      await expect(p).rejects.toThrowError(new Error('Missing request timeout context'));
    });

    it('rejects Promise upon timeout updating context', async () => {
      const ctx = { requestTimeout: { hasTimedOut: false } } as Context;
      const p = timeoutPromise(ctx, TIMEOUT_IN_MS);

      jest.runAllTimers();

      await expect(p).rejects.toThrowError();
      expect(ctx.requestTimeout && ctx.requestTimeout.hasTimedOut).toBe(true);
    });
  });

  describe('clearRequestTimeout', () => {
    it('should clear request timeout when in context', () => {
      const timer = setTimeout(() => null, 0);
      const ctx = { requestTimeout: { timer } } as Context;

      clearRequestTimeout(ctx);
      expect(clearTimeout).toHaveBeenCalledTimes(1);
      expect(clearTimeout).toHaveBeenCalledWith(timer);
    });

    it("shouldn't clear request timeout without context", () => {
      const ctx = {} as Context;

      clearRequestTimeout(ctx);
      expect(clearTimeout).toHaveBeenCalledTimes(0);
    });
  });

  describe('createRequestTimeoutMiddleware', () => {
    it('returns factory for creating requestTimeoutMiddleware', () => {
      const middleware = createRequestTimeoutMiddleware(getMockConfig());

      expect(typeof middleware).toBe('function');
    });

    it('should call nextFn on successful invocation', async () => {
      const ctx = {} as Context;
      const nextFn = jest.fn().mockImplementation(() => Promise.resolve(true));

      const reqTimeoutMiddleware = createRequestTimeoutMiddleware(getMockConfig());

      // run all timers
      jest.runAllTimers();

      // execute middleware
      await reqTimeoutMiddleware(ctx, nextFn);

      expect(nextFn).toHaveBeenCalled();
      expect(ctx.requestTimeout && ctx.requestTimeout.hasTimedOut).toBe(false);
      expect(clearTimeout).toHaveBeenCalledTimes(1);
    });

    it('should return http 504 on request timeout', async () => {
      const ctx = {} as Context;

      // next middleware runs twice as long as fakeTimeoutInMs
      const nextFn = jest
        .fn()
        .mockImplementation(() => new Promise((resolve) => setTimeout(resolve, 2 * TIMEOUT_IN_MS)));

      const reqTimeoutMiddleware = createRequestTimeoutMiddleware(getMockConfig());

      try {
        // execute middleware
        await Promise.all([reqTimeoutMiddleware(ctx, nextFn), jest.runAllTimers()]);
      } catch (err) {
        expect(nextFn).toHaveBeenCalled();
        expect(err).toBeTruthy();
        expect(err).toBeInstanceOf(RequestTimeoutError);
        expect(err.status).toBe(TIMEOUT_HTTP_STATUS);
        expect(err.expose).toBe(false);
        expect(err.message).toBe(`Request timed out after ${TIMEOUT_IN_MS} ms`);
        expect(ctx.requestTimeout && ctx.requestTimeout.hasTimedOut).toBe(true);
      }
    });

    it('should cascade middleware internal errors', async () => {
      const ctx = {
        throw: (status: number, message: string) => {
          const err = new Error(message);
          (err as any).status = status;
          throw err;
        }
      } as Context;

      // next middleware throw an error
      const nextFn = jest.fn().mockImplementation(async () => {
        ctx.throw(519, 'fancy http error that does not exist');
      });

      const reqTimeoutMiddleware = createRequestTimeoutMiddleware(getMockConfig());

      /// advance timers by 1ms
      jest.advanceTimersByTime(1);

      try {
        // execute middleware
        await reqTimeoutMiddleware(ctx, nextFn);
      } catch (err) {
        expect(nextFn).toHaveBeenCalled();
        expect(err).toBeTruthy();
        expect(err.status).toBe(519);
        expect(err.message).toBe('fancy http error that does not exist');
        expect(ctx.requestTimeout && ctx.requestTimeout.hasTimedOut).toBe(false);
      }
    });
  });
});
